package com.example.aws.secretmanager;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AwsSecrectmanagerApplication implements CommandLineRunner {

	@Value(value = "${spring.datasource.url}")
	private String url;

	@Value(value = "${spring.datasource.username}")
	private String username;

	@Value(value = "${spring.datasource.password}")
	private String password;


	public static void main(String[] args) {
		SpringApplication.run(AwsSecrectmanagerApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		System.out.println("url : "+ url);
		System.out.println("username : "+ username);
		System.out.println("password : "+ password);
	}
}
