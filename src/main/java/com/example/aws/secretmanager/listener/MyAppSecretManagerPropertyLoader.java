package com.example.aws.secretmanager.listener;

import com.amazonaws.services.secretsmanager.AWSSecretsManager;
import com.amazonaws.services.secretsmanager.AWSSecretsManagerClientBuilder;
import com.amazonaws.services.secretsmanager.model.*;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.event.ApplicationEnvironmentPreparedEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.PropertiesPropertySource;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.util.Base64;
import java.util.Properties;

public class MyAppSecretManagerPropertyLoader implements ApplicationListener<ApplicationEnvironmentPreparedEvent> {
    private static final Logger logger = LoggerFactory.getLogger(MyAppSecretManagerPropertyLoader.class);

    private ObjectMapper mapper = new ObjectMapper();

    @Override
    public void onApplicationEvent(ApplicationEnvironmentPreparedEvent event) {
        logger.info("Executing Application Event Listener.");


        boolean secretmanagerEnabled = false;

        // TODO: read this form env_variable

        final String strSecretmanagerEnabled = "true";
        /*
                System.getenv(Constants.LORAX_SECRET_MANAGER_ENABLED) != null
                ? System.getenv(Constants.LORAX_SECRET_MANAGER_ENABLED)
                : System.getenv(Constants.LORAX_SECRET_MANAGER_ENABLED2);
         */

        if (!StringUtils.isEmpty(strSecretmanagerEnabled)) {
            secretmanagerEnabled = Boolean.valueOf(strSecretmanagerEnabled);
        }
        logger.info("Secret Manager Enabled : " + secretmanagerEnabled);

        // logger.info("Secret Manager Enabled : " + secretmanagerEnabled);
        if (secretmanagerEnabled) {
            // Get username and password from AWS Secret Manager using secret name
            String secretJson = getSecret();

            // reading from ssm keys
            String datasourceUrl = getString(secretJson, "spring.datasource.url");
            String datasourceUsername = getString(secretJson, "spring.datasource.username");
            String datasourcePassword = getString(secretJson, "spring.datasource.password");
            ConfigurableEnvironment environment = event.getEnvironment();

            Properties props = new Properties();

            // updating properties from ssm
            props.put("spring.datasource.url", datasourceUrl);
            props.put("spring.datasource.username", datasourceUsername);
            props.put("spring.datasource.password", datasourcePassword);

            // ?
            environment
                    .getPropertySources()
                    .addFirst(new PropertiesPropertySource("aws.secret.manager", props));

        }
        logger.info("Execution Application Event Listener Completed.");
    }

    // From Vivek
    private String getSecret() {

        final String secretName = System.getenv("secret.name");
        String region = System.getenv("aws.region");

        if (region == null || region.trim().isEmpty()) {
            logger.info("========not available in evn falling back==================AWS Region ");
            region = "us-east-2";
        }
        // String region = "us-east-2";
        logger.info("==========================AWS Region "+region);

        logger.info("Reading Secret Manager : " + secretName);

        if (!StringUtils.isEmpty(secretName)) {
            // Create a Secrets Manager client
            AWSSecretsManager client = AWSSecretsManagerClientBuilder
                    .standard()
                    .withRegion(region)
                    .build();

            // In this sample we only handle the specific exceptions for the
            // 'GetSecretValue' API.
            // See
            // https://docs.aws.amazon.com/secretsmanager/latest/apireference/API_GetSecretValue.html
            // We rethrow the exception by default.

            String secret = null, decodedBinarySecret = null;
            GetSecretValueRequest getSecretValueRequest = new GetSecretValueRequest().withSecretId(secretName);
            GetSecretValueResult getSecretValueResult = null;

            try {
                getSecretValueResult = client.getSecretValue(getSecretValueRequest);
            } catch (DecryptionFailureException e) {
                // Secrets Manager can't decrypt the protected secret text using the provided
                // KMS key.
                // Deal with the exception here, and/or rethrow at your discretion.
                logger.error("Error while getting Secret from AWS : " + e.getMessage());
                throw e;
            } catch (InternalServiceErrorException e) {
                // An error occurred on the server side.
                // Deal with the exception here, and/or rethrow at your discretion.
                logger.error("Error while getting Secret from AWS : " + e.getMessage());
                throw e;
            } catch (InvalidParameterException e) {
                // You provided an invalid value for a parameter.
                // Deal with the exception here, and/or rethrow at your discretion.
                logger.error("Error while getting Secret from AWS : " + e.getMessage());
                throw e;
            } catch (InvalidRequestException e) {
                // You provided a parameter value that is not valid for the current state of the
                // resource.
                // Deal with the exception here, and/or rethrow at your discretion.
                logger.error("Error while getting Secret from AWS : " + e.getMessage());
                throw e;
            } catch (ResourceNotFoundException e) {
                // We can't find the resource that you asked for.
                // Deal with the exception here, and/or rethrow at your discretion.
                logger.error("Error while getting Secret from AWS : " + e.getMessage());
                throw e;
            } catch (Exception e) {
                // We can't find the resource that you asked for.
                // Deal with the exception here, and/or rethrow at your discretion.
                logger.error("Error  : " + e.getMessage());
                throw e;
            }

            // Decrypts secret using the associated KMS CMK.
            // Depending on whether the secret is a string or binary, one of these fields
            // will be populated.
            if (getSecretValueResult.getSecretString() != null) {
                secret = getSecretValueResult.getSecretString();
            } else {
                decodedBinarySecret = new String(
                        Base64.getDecoder().decode(getSecretValueResult.getSecretBinary()).array());
            }
            return secret != null ? secret : decodedBinarySecret;
        }

        // If we reach here. AWS connection was success but nothing configured within
        // Secret Manager
        logger.info("Nothing configured within AWS Secret Manager ! ");
        return "";
    }

    private String getString(String json, String path) {
        try {
            JsonNode root = mapper.readTree(json);
            return root.path(path).asText();
        } catch (IOException e) {
            logger.error("Can't get {} from json {}", path, json, e);
            return null;
        }
    }
}
