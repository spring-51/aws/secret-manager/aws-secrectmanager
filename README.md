# AWS SECRET MANAGER
Reads secrets like db url, credentials from aws.


#### Steps
```
Step 1 : Add pom dependency as
    <dependency>
        <groupId>com.amazonaws</groupId>
        <artifactId>aws-java-sdk-secretsmanager</artifactId>
        <version>1.11.415</version>
    </dependency>

Step 2 :
    Create META-INF directory under src/main/resources

Step 3 : Create  spring.factories file under META-INF
Step 4 : Add fully classified name of class which reads secrets from aws secret manager
         and override them in the app
         in our case it is
         org.springframework.context.ApplicationListener=com.example.aws.secretmanager.listener.MyAppSecretManagerPropertyLoader

Step 5 : Add code changes to read secrets from aws and override them in MyAppSecretManagerPropertyLoader
         refer - MyAppSecretManagerPropertyLoader.java

Step 6 : add aws user creds in %userprofile%/.aws/credential file with default profile
    [default]
    aws_access_key_id=<access-key>
    aws_secret_access_key=<aws-secret-access-key>

Step 7 : Add below env variable(since we used these in MyAppSecretManagerPropertyLoader ) in Intellij Run Config.
  - secret.name=<name-of-aws-secret-manager>
  - aws.region=<region-of-aws-security-manager>

Step 8 : Run and Test the properties value.
       - refer  - AwsSecrectmanagerApplication -> run(String... args)
```
